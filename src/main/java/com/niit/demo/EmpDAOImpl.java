package com.niit.demo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("service1")
public class EmpDAOImpl implements EmpDAO {

	@Autowired
	MyRepository myrepo;
	
	@Override
	public void addEmp(Employee emp) {
		myrepo.save(emp);
		
	}

	@Override
	public List getAllemployee() {
	 
		
		return myrepo.findAll();
	}

	@Override
	public Employee findEmployee(String ename) {
		 
		Employee empboj=myrepo.findByEmpname(ename);
		return empboj;
	}

}
