package com.niit.demo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainController 
{
@Autowired
EmpDAO service1;

@GetMapping()
public ResponseEntity<String> first()
{
	return new ResponseEntity<String>("welcome",HttpStatus.OK);
}

@PostMapping("/addrec")
public ResponseEntity<?> addEmployee(@RequestBody Employee employee)
{
 service1.addEmp(employee);
 return new ResponseEntity<String>("Added",HttpStatus.OK);
}

@GetMapping("/show")

public ResponseEntity<?> showall ( )
{
	List list=service1.getAllemployee();
	
	return new ResponseEntity<List>(list,HttpStatus.OK);
	
}

}
