package com.niit.demo;

import java.util.List;

public interface EmpDAO {

	public void addEmp(Employee emp);
	public List getAllemployee();
	public Employee findEmployee(String ename);
}
