FROM java:8-jre
WORKDIR usr/src
ENV MYSQL_DATABASE=demodb
ENV MYSQL_PASSWORD=password
ENV MYSQL_ROOT_PASSWORD=password
ENV MYSQL_CI_URL=jdbc:mysql://localhost:3306/demodb?createDatabaseIfNotExist=true
ADD ./target/JPAExample-0.0.1-SNAPSHOT.jar /usr/src/JPAExample-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","JPAExample-0.0.1-SNAPSHOT.jar"]